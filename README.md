## Project Request
As an enduser I want to have a small website, which displays various data of Bundesliga games using the JSON/XML API http://www.openligadb.de/  (see ​https://bitbucket.org/breudan/interview​).
 The company is planning on adding offline functionality, extensive searching capabilities, and the possibility to add Notes and Imagedata to each Match, Gameday, Season and Team in future versions of the project you will provide.

Provide the following Information:

- Next upcoming matches (following Gameday)
- All matches of the actual season
- Win/Loss Ratio of the actual season of each team
- Search functionality:
    - Search for a Goalgetters Name will provide the count of goals he made in the current season

Bonus-Points:

- Automated deployment script with fabric/ansible or the like.    If you do so, deployment Target should be a local Vagrantbox.

Please describe why you choose which technology (Framework, Language, SOAP/JSON), how you evaluated what to do, and provide a description of your Workflow to solve this problem.

A link to a repository with a small Readme.md for setup is needed.

You may use a persistence layer of your choice if you feel the need to (mongoDB, mySQL p.e)

The actual Design of the Site is totally up to you, functionality and proper design of the Backend should be your focus. Nice Web-Frontend is a bonus but not a must.

## Description
   The project is realised with symfony 4.1. The framework is quite mature. My experience for the last years is on symfony, too.

   The XML API was chosen although it was not straight forward working with it. With commands (like cron jobs) the Data is taken from the API and inserted in MySQL DB.

   I have created a DB design to save data from the API. The idea is that this project will evolve into more complex and the XML API is just a fraction of the Data.

   The project has User functionality although all links are public.

   Twitter Bootstrap is used for the front-end.

   The ansible script in `ansible/playbook.yml` is an example as I don't have time and space to place Vagrant on my laptop. Anyway I wrote detailed installation guide for the project. It is working with all the requested features plus smooth front-end.

![project screenshot][screenshot_01]

**Static analysis of the project**
![project screenshot][screenshot_02]

## Installation
1. Clone the project from the repo
    ```
    git clone https://tgeorgiev@bitbucket.org/tgeorgiev/bundesliga-fanbase.git
    ```
2. enter in the project `cd bundesliga-fanbase`
3. install everything via composer `composer install`
4. update `.env` file with DATABASE_URL like
    ```
    DATABASE_URL=mysql://root:@127.0.0.1:3306/bundesliga
    ```
4. create the DB `bundesliga` with `php bin/console doctrine:database:create`
5. migrate the DB `php bin/console doctrine:migrations:migrate`
6. start a server `php bin/console server:run`
7. get Match Data through the XML API. Run the following commands
    ```
    php bin/console app:getTeams bl1 2018
    php bin/console app:getTeamResults bl1 2018
    php bin/console app:getPlayerResults bl1 2018
    php bin/console app:getGroups bl1 2018
    php bin/console app:getMatches bl1 2018
    ```

## Useful Commands
* Create a test user via the console
    ```
    php bin/console fos:user:create testuser
    ```
* promote a user with admin role
    ```
    php bin/console fos:user:promote testuser ROLE_ADMIN
    ```
* run unit tests. You should have created one user in order to pass all tests. You should also setup `DATABASE_URL` in `phpunit.xml.dist`
    ```
    ./bin/phpunit --log-junit 'reports/unitreport.xml' --coverage-clover 'reports/clover.xml' --coverage-html 'reports/clover_html' tests/
    ```

## Useful Links
* All matches (home page) [http://127.0.0.1:8000](http://127.0.0.1:8000)
* Upcoming matches [http://127.0.0.1:8000/upcoming](http://127.0.0.1:8000/upcoming)
* Team season stats [http://127.0.0.1:8000/team/results](http://127.0.0.1:8000/team/results)
* Player search [http://127.0.0.1:8000/player/search?name=Müller](http://127.0.0.1:8000/player/search?name=Müller)
* User login [http://127.0.0.1:8000/login](http://127.0.0.1:8000/login) *the user can login with username or email*

![project screenshot][screenshot_03]


[screenshot_01]: https://bitbucket.org/tgeorgiev/bundesliga-fanbase/raw/master/public/img/screenshot_01.png "Screenshot"
[screenshot_02]: https://bitbucket.org/tgeorgiev/bundesliga-fanbase/raw/master/public/img/screenshot_02.png "Screenshot"
[screenshot_03]: https://bitbucket.org/tgeorgiev/bundesliga-fanbase/raw/master/public/img/screencast.gif "Screencast"