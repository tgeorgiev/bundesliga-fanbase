<?php

namespace App\Controller;


use App\Entity\Match;
use App\Repository\GroupRepository;
use App\Repository\MatchRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller used to view matches in the public part of the site.
 *
 */
class MatchController extends AbstractController
{
    /**
     * @Route("/", name="match_index")
     */
    public function index(Request $request, MatchRepository $matchRepo): Response
    {
        $matches = $matchRepo->findLatest();
        return $this->render('matches.html.twig', ['matches' => $matches]);
    }

    /**
     * @Route("/upcoming", name="match_upcoming")
     */
    public function upcoming(Request $request, MatchRepository $matchRepo, GroupRepository $groupRepo): Response
    {
        $currentGroup = $groupRepo->findOneBy(['liga'=>'bl1','current'=>1]);

        return $this->render('matches.html.twig', ['matches' => $matchRepo->findUpcoming($currentGroup)]);
    }

    /**
     * @Route("/match/{matchId}", methods={"GET"}, name="match_view")
     */
    public function matchView(Match $match): Response
    {
        return $this->render('matchView.html.twig', ['match' => $match]);
    }

}