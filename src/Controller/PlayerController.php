<?php

namespace App\Controller;

use App\Repository\PlayerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/player", name="player_")
 */
class PlayerController extends AbstractController
{
    /**
     * @Route("/player", name="player")
     */
    public function index()
    {
        return $this->render('player/index.html.twig', [
            'controller_name' => 'PlayerController',
        ]);
    }

    /**
     * @Route("/search", name="search")
     */
    public function search(PlayerRepository $playerRepo, Request $request)
    {
        /*$players = $playerRepo->findBy(
            ['name' => $request->get('name')]
        );*/

        $players = $playerRepo->searchByName($request->get('name'));

        return $this->render('player/searchResults.html.twig', [
            'controller_name'   => 'PlayerController',
            'players'       => $players,
        ]);
    }
}
