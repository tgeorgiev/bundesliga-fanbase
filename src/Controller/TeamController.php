<?php

namespace App\Controller;

use App\Repository\TeamResultRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/team", name="team_")
 */
class TeamController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('team/index.html.twig', [
            'controller_name' => 'TeamController',
        ]);
    }

    /**
     * @Route("/results", name="results")
     */
    public function results(TeamResultRepository $teamResultRepo)
    {
        $results = $teamResultRepo->findBy(array('year' => date("Y")), array('liga'=>'ASC','points'=>'DESC'));
        return $this->render('team/results.html.twig', [
            'controller_name'   => 'TeamController',
            'teamResults'       => $results,
        ]);
    }
}
