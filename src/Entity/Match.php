<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MatchRepository")
 * @ORM\Table(name="liga_match")
 * @UniqueEntity("matchId")
 */
class Match
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Group", inversedBy="matches")
     */
    private $ligaGroup;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $dateUTC;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $matchId;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isFinished;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $viewersNumber;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Team")
     * @ORM\JoinColumn(nullable=false)
     */
    private $team1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Team")
     * @ORM\JoinColumn(nullable=false)
     */
    private $team2;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $liga;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $team1Goals;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $team2Goals;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLigaGroup(): ?Group
    {
        return $this->ligaGroup;
    }

    public function setLigaGroup(?Group $ligaGroup): self
    {
        $this->ligaGroup = $ligaGroup;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDateUTC(): ?\DateTimeInterface
    {
        return $this->dateUTC;
    }

    public function setDateUTC(\DateTimeInterface $dateUTC): self
    {
        $this->dateUTC = $dateUTC;

        return $this;
    }

    public function getMatchId(): ?int
    {
        return $this->matchId;
    }

    public function setMatchId(int $matchId): self
    {
        $this->matchId = $matchId;

        return $this;
    }

    public function getIsFinished(): ?bool
    {
        return $this->isFinished;
    }

    public function setIsFinished(bool $isFinished): self
    {
        $this->isFinished = $isFinished;

        return $this;
    }

    public function getViewersNumber(): ?int
    {
        return $this->viewersNumber;
    }

    public function setViewersNumber(?int $viewersNumber): self
    {
        $this->viewersNumber = $viewersNumber;

        return $this;
    }

    public function getTeam1(): ?Team
    {
        return $this->team1;
    }

    public function setTeam1(?Team $team1): self
    {
        $this->team1 = $team1;

        return $this;
    }

    public function getTeam2(): ?Team
    {
        return $this->team2;
    }

    public function setTeam2(?Team $team2): self
    {
        $this->team2 = $team2;

        return $this;
    }

    public function getLiga(): ?string
    {
        return $this->liga;
    }

    public function setLiga(string $liga): self
    {
        $this->liga = $liga;

        return $this;
    }

    public function getTeam1Goals(): ?int
    {
        return $this->team1Goals;
    }

    public function setTeam1Goals(?int $team1Goals): self
    {
        $this->team1Goals = $team1Goals;

        return $this;
    }

    public function getTeam2Goals(): ?int
    {
        return $this->team2Goals;
    }

    public function setTeam2Goals(?int $team2Goals): self
    {
        $this->team2Goals = $team2Goals;

        return $this;
    }
}
