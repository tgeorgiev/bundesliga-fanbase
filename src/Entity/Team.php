<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TeamRepository")
 * @UniqueEntity("teamId")
 */
class Team
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $shortName;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $teamGroupName;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $teamIconUrl;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $teamId;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $liga;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TeamResult", mappedBy="teamId")
     */
    private $teamResults;

    public function __construct()
    {
        $this->teamResults = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function setShortName(string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }

    public function getTeamGroupName(): ?string
    {
        return $this->teamGroupName;
    }

    public function setTeamGroupName(?string $teamGroupName): self
    {
        $this->teamGroupName = $teamGroupName;

        return $this;
    }

    public function getTeamIconUrl(): ?string
    {
        return $this->teamIconUrl;
    }

    public function setTeamIconUrl(?string $teamIconUrl): self
    {
        $this->teamIconUrl = $teamIconUrl;

        return $this;
    }

    public function getTeamId(): ?int
    {
        return $this->teamId;
    }

    public function setTeamId(int $teamId): self
    {
        $this->teamId = $teamId;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLiga(): ?string
    {
        return $this->liga;
    }

    public function setLiga(string $liga): self
    {
        $this->liga = $liga;

        return $this;
    }

    /**
     * @return Collection|TeamResult[]
     */
    public function getTeamResults(): Collection
    {
        return $this->teamResults;
    }

    public function addTeamResult(TeamResult $teamResult): self
    {
        if (!$this->teamResults->contains($teamResult)) {
            $this->teamResults[] = $teamResult;
            $teamResult->setTeamId($this);
        }

        return $this;
    }

    public function removeTeamResult(TeamResult $teamResult): self
    {
        if ($this->teamResults->contains($teamResult)) {
            $this->teamResults->removeElement($teamResult);
            // set the owning side to null (unless already changed)
            if ($teamResult->getTeamId() === $this) {
                $teamResult->setTeamId(null);
            }
        }

        return $this;
    }
}
