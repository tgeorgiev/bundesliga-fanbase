<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\Player;
use App\Entity\PlayerStat;
use GuzzleHttp\Client;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class GetPlayerResultsCommand extends Command
{
    protected static $defaultName = 'app:getPlayerResults';
    private $em;
    private $openligaClient;

    public function __construct(EntityManagerInterface $entityManager, Client $openligaClient)
    {
        parent::__construct();
        $this->em = $entityManager;
        $this->openligaClient = $openligaClient;
    }

    protected function configure()
    {
        $this
            ->setDescription('Get player results from OpenLigaDB https://www.openligadb.de/api/getgoalgetters/bl1/2018')
            ->addArgument('arg1', InputArgument::REQUIRED, 'Bundes Liga. ex. bl1')
            ->addArgument('arg2', InputArgument::REQUIRED, 'Year')
            //->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');
        $arg2 = $input->getArgument('arg2');

        $response = $this->openligaClient->get(sprintf('getgoalgetters/%s/%d', $arg1, $arg2));

        $responseXml = simplexml_load_string($response->getBody()->getContents());
        $playerResults = $responseXml->children();

        $i = 1;
        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        try {

            foreach ($playerResults as $res) {
                $playerRepo = $this->em->getRepository(Player::class);
                $player = $playerRepo->findOneBy(['playerId'=>$res->GoalGetterId]);

                if(!$player) {
                    $player = new Player();
                    $player->setPlayerId((int)$res->GoalGetterId);
                    $player->setName($res->GoalGetterName);
                    $this->em->persist($player);
                    //$io->note(sprintf('Missing Player %s with id:%d', $res->GoalGetterName, $res->GoalGetterId));
                }

                $playerEn = new PlayerStat();
                $playerEn->setPlayer($player);
                $playerEn->setGoals((int)$res->GoalCount);
                $playerEn->setLiga($arg1);
                $playerEn->setYear((int)$arg2);
                $this->em->persist($playerEn);
                $i++;

            }

            $this->em->flush();
            $this->em->getConnection()->commit();
            $io->success(sprintf('%d player stats inserted to DB', $i));
        } catch (Exception $e) {
            $this->em->getConnection()->rollBack();
            $io->error(sprintf('%s', $e->getMessage()));
        }

        $this->em->clear();
    }
}
