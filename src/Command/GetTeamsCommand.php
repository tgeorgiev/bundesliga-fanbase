<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\Team;
use GuzzleHttp\Client;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class GetTeamsCommand extends Command
{
    protected static $defaultName = 'app:getTeams';
    private $em;
    private $openligaClient;

    public function __construct(EntityManagerInterface $entityManager, Client $openligaClient)
    {
        parent::__construct();
        // best practices recommend to call the parent constructor first and
        // then set your own properties. That wouldn't work in this case
        // because configure() needs the properties set in this constructor
        $this->em = $entityManager;
        $this->openligaClient = $openligaClient;
    }

    protected function configure()
    {
        $this
            ->setDescription('Get Teams from OpenLigaDB https://www.openligadb.de/api/getavailableteams/bl1/2018')
            ->addArgument('arg1', InputArgument::REQUIRED, 'Bundes Liga. ex. bl1')
            ->addArgument('arg2', InputArgument::REQUIRED, 'Year')
            //->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');
        $arg2 = $input->getArgument('arg2');

        /*if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }*/

        $response = $this->openligaClient->get(sprintf('getavailableteams/%s/%d', $arg1, $arg2));

        $responseXml = simplexml_load_string($response->getBody()->getContents());
        $teams = $responseXml->children();

        //$batchSize = 20;
        $i = 1;
        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        try {

            foreach ($teams as $team) {
                $teamEn = new Team();
                $teamEn->setShortName($team->ShortName);
                $teamEn->setTeamGroupName($team->TeamGroupName);
                $teamEn->setTeamIconUrl($team->TeamIconUrl);
                $teamEn->setTeamId((int)$team->TeamId);
                $teamEn->setName($team->TeamName);
                $teamEn->setLiga($arg1);
                $this->em->persist($teamEn);
                /*if (($i % $batchSize) === 0) {
                    $this->em->flush();
                    $this->em->clear(); // Detaches all objects from Doctrine!
                }*/
                $i++;
            }

            //$this->em->flush(); //Persist objects that did not make up an entire batch
            //$this->em->clear();

            $this->em->flush();
            $this->em->getConnection()->commit();
            $io->success(sprintf('%d teams inserted to DB', $i));
        } catch (Exception $e) {
            $this->em->getConnection()->rollBack();
            $io->error(sprintf('%s', $e->getMessage()));
        }

        $this->em->clear();

        //if ($input->getOption('option1')) {
            // ...
        //}
    }
}
