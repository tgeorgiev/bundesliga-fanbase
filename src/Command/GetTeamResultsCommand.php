<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\TeamResult;
use App\Entity\Team;
use App\Repository\TeamResultRepository;
use GuzzleHttp\Client;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class GetTeamResultsCommand extends Command
{
    protected static $defaultName = 'app:getTeamResults';
    private $em;
    private $openligaClient;

    public function __construct(EntityManagerInterface $entityManager, Client $openligaClient)
    {
        parent::__construct();
        $this->em = $entityManager;
        $this->openligaClient = $openligaClient;
    }

    protected function configure()
    {
        $this
            ->setDescription('Get team results from OpenLigaDB https://www.openligadb.de/api/getbltable/bl1/2018')
            ->addArgument('arg1', InputArgument::REQUIRED, 'Bundes Liga. ex. bl1')
            ->addArgument('arg2', InputArgument::REQUIRED, 'Year')
            //->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');
        $arg2 = $input->getArgument('arg2');

        $response = $this->openligaClient->get(sprintf('getbltable/%s/%d', $arg1, $arg2));

        $responseXml = simplexml_load_string($response->getBody()->getContents());
        $teamResults = $responseXml->children();

        $i = 1;
        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        try {

            foreach ($teamResults as $res) {
                $teamRepo = $this->em->getRepository(Team::class);
                $team = $teamRepo->findOneBy(['teamId'=>$res->TeamInfoId]);
                if($team) {
                    $teamEn = new TeamResult();
                    $teamEn->setTeam($team);
                    $teamEn->setDraw((int)$res->Draw);
                    $teamEn->setGoals((int)$res->Goals);
                    $teamEn->setLost((int)$res->Lost);
                    $teamEn->setMatches((int)$res->Matches);
                    $teamEn->setOpponentGoals((int)$res->OpponentGoals);
                    $teamEn->setPoints((int)$res->Points);
                    $teamEn->setWon((int)$res->Won);
                    $teamEn->setLiga($arg1);
                    $teamEn->setYear((int)$arg2);
                    $this->em->persist($teamEn);
                    $i++;
                } else {
                    $io->note(sprintf('Missing Team %s with id:%d', $res->TeamName, $res->TeamInfoId));
                }

            }

            $this->em->flush();
            $this->em->getConnection()->commit();
            $io->success(sprintf('%d team results inserted to DB', $i));
        } catch (Exception $e) {
            $this->em->getConnection()->rollBack();
            $io->error(sprintf('%s', $e->getMessage()));
        }

        $this->em->clear();
    }
}
