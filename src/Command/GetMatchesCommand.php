<?php

namespace App\Command;

use App\Entity\Group;
use App\Entity\Team;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\Match;
use GuzzleHttp\Client;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use DateTime;

class GetMatchesCommand extends Command
{
    protected static $defaultName = 'app:getMatches';
    private $em;
    private $openligaClient;

    public function __construct(EntityManagerInterface $entityManager, Client $openligaClient)
    {
        parent::__construct();
        $this->em = $entityManager;
        $this->openligaClient = $openligaClient;
    }

    protected function configure()
    {
        $this
            ->setDescription('Get Matches from OpenLigaDB https://www.openligadb.de/api/getmatchdata/bl1/2018')
            ->addArgument('arg1', InputArgument::REQUIRED, 'Bundes Liga. ex. bl1')
            ->addArgument('arg2', InputArgument::REQUIRED, 'Year')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');
        $arg2 = $input->getArgument('arg2');

        $response = $this->openligaClient->get(sprintf('getmatchdata/%s/%d', $arg1, $arg2));

        $responseXml = simplexml_load_string($response->getBody()->getContents());
        $matches = $responseXml->children();

        $i = 1;
        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        $groupRepo = $this->em->getRepository(Group::class);
        $teamRepo = $this->em->getRepository(Team::class);
        try {
            foreach ($matches as $match) {
                $group = $groupRepo->findOneBy(['groupId'=>$match->Group->GroupID]);
                $team1 = $teamRepo->findOneBy(['teamId'=>$match->Team1->TeamId]);

                $team2 = $teamRepo->findOneBy(['teamId'=>$match->Team2->TeamId]);
                $matchEn = new Match();
                $matchEn->setMatchId((int)$match->MatchID);
                if ($group) {
                    $matchEn->setLigaGroup($group);
                }
                $matchEn->setUpdatedAt(new DateTime($match->LastUpdateDateTime));
                $matchEn->setDate(new DateTime($match->MatchDateTime));
                $matchEn->setDateUTC(new DateTime($match->MatchDateTimeUTC));
                $matchEn->setMatchId((int)$match->MatchID);
                if (empty((bool)$match->MatchIsFinished)) {
                    $matchEn->setIsFinished(0);
                } else {
                    $matchEn->setIsFinished(1);
                }
                $matchEn->setViewersNumber((int)$match->NumberOfViewers);
                if (isset($match->MatchResults->children()[0])) {
                    $matchEn->setTeam1Goals((int)$match->MatchResults->children()[0]->PointsTeam1);
                    $matchEn->setTeam2Goals((int)$match->MatchResults->children()[0]->PointsTeam2);
                }
                if ($team1 && $team2) {
                    $matchEn->setTeam1($team1);
                    $matchEn->setTeam2($team2);
                }
                $matchEn->setLiga($arg1);
                $this->em->persist($matchEn);
                $i++;
            }

            $this->em->flush();
            $this->em->getConnection()->commit();
            $io->success(sprintf('%d matches inserted to DB', $i));
        } catch (Exception $e) {
            $this->em->getConnection()->rollBack();
            $io->error(sprintf('%s', $e->getMessage()));
        }

        $this->em->clear();
    }
}
