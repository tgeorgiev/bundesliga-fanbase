<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\Group;
use GuzzleHttp\Client;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class GetGroupsCommand extends Command
{
    protected static $defaultName = 'app:getGroups';
    private $em;
    private $openligaClient;

    public function __construct(EntityManagerInterface $entityManager, Client $openligaClient)
    {
        parent::__construct();
        $this->em = $entityManager;
        $this->openligaClient = $openligaClient;
    }

    protected function configure()
    {
        $this
            ->setDescription('Get Groups from OpenLigaDB https://www.openligadb.de/api/getavailablegroups/bl1/2018')
            ->addArgument('arg1', InputArgument::REQUIRED, 'Bundes Liga. ex. bl1')
            ->addArgument('arg2', InputArgument::REQUIRED, 'Year')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');
        $arg2 = $input->getArgument('arg2');

        $response = $this->openligaClient->get(sprintf('getavailablegroups/%s/%d', $arg1, $arg2));

        $responseXml = simplexml_load_string($response->getBody()->getContents());
        $groups = $responseXml->children();

        //get current group in bundesliga
        $response = $this->openligaClient->get(sprintf('getcurrentgroup/%s', $arg1));
        $responseXml = simplexml_load_string($response->getBody()->getContents());
        $currentGroupID = (int)$responseXml->GroupID;

        $i = 1;
        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        try {

            foreach ($groups as $group) {
                $groupEn = new Group();
                $groupEn->setGroupId((int)$group->GroupID);
                $groupEn->setName($group->GroupName);
                $groupEn->setOrderId((int)$group->GroupOrderID);
                $groupEn->setLiga($arg1);
                $groupEn->setYear((int)$arg2);
                if ($group->GroupID == $currentGroupID) {
                    $groupEn->setCurrent(1);
                } else {
                    $groupEn->setCurrent(0);
                }
                $this->em->persist($groupEn);
                $i++;
            }

            $this->em->flush();
            $this->em->getConnection()->commit();
            $io->success(sprintf('%d groups inserted to DB', $i));
        } catch (Exception $e) {
            $this->em->getConnection()->rollBack();
            $io->error(sprintf('%s', $e->getMessage()));
        }

        $this->em->clear();
    }
}
