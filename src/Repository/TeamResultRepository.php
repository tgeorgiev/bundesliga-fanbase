<?php

namespace App\Repository;

use App\Entity\TeamResult;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TeamResult|null find($id, $lockMode = null, $lockVersion = null)
 * @method TeamResult|null findOneBy(array $criteria, array $orderBy = null)
 * @method TeamResult[]    findAll()
 * @method TeamResult[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeamResultRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TeamResult::class);
    }

//    /**
//     * @return TeamResult[] Returns an array of TeamResult objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TeamResult
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
