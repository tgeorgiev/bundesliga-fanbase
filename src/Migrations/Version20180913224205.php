<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180913224205 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE liga_group (id INT AUTO_INCREMENT NOT NULL, group_id INT NOT NULL, name VARCHAR(255) NOT NULL, order_id INT NOT NULL, liga VARCHAR(4) NOT NULL, year INT NOT NULL, current TINYINT(1) DEFAULT NULL, UNIQUE INDEX UNIQ_79445C7EFE54D947 (group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE player (id INT AUTO_INCREMENT NOT NULL, player_id INT NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_98197A6599E6F5DF (player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE player_stat (id INT AUTO_INCREMENT NOT NULL, player_id INT NOT NULL, goals INT DEFAULT NULL, liga VARCHAR(4) NOT NULL, year INT NOT NULL, INDEX IDX_82A2AF1299E6F5DF (player_id), UNIQUE INDEX UNIQ_34A0E63489D9B62 (player_id,liga,year), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team (id INT AUTO_INCREMENT NOT NULL, short_name VARCHAR(100) DEFAULT NULL, team_group_name VARCHAR(100) DEFAULT NULL, team_icon_url LONGTEXT DEFAULT NULL, team_id INT NOT NULL, team_name VARCHAR(200) NOT NULL, liga VARCHAR(4) NOT NULL, UNIQUE INDEX UNIQ_C4E0A61F296CD8AE (team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team_result (id INT AUTO_INCREMENT NOT NULL, team_id INT NOT NULL, draw INT DEFAULT NULL, goals INT DEFAULT NULL, lost INT DEFAULT NULL, matches INT DEFAULT NULL, opponent_goals INT DEFAULT NULL, points INT DEFAULT NULL, won INT DEFAULT NULL, liga VARCHAR(4) NOT NULL, year INT NOT NULL, INDEX IDX_657186CA296CD8AE (team_id), UNIQUE INDEX UNIQ_23A0E63489D9B62 (team_id,year), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE player_stat ADD CONSTRAINT FK_82A2AF1299E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE team_result ADD CONSTRAINT FK_657186CA296CD8AE FOREIGN KEY (team_id) REFERENCES team (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE player_stat DROP FOREIGN KEY FK_82A2AF1299E6F5DF');
        $this->addSql('ALTER TABLE team_result DROP FOREIGN KEY FK_657186CA296CD8AE');
        $this->addSql('DROP TABLE liga_group');
        $this->addSql('DROP TABLE player');
        $this->addSql('DROP TABLE player_stat');
        $this->addSql('DROP TABLE team');
        $this->addSql('DROP TABLE team_result');
    }
}
