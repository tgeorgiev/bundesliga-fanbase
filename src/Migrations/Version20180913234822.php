<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180913234822 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `liga_match` (id INT AUTO_INCREMENT NOT NULL, liga_group_id INT DEFAULT NULL, team1_id INT NOT NULL, team2_id INT NOT NULL, updated_at DATETIME NOT NULL, date DATETIME NOT NULL, date_utc DATETIME NOT NULL, match_id INT NOT NULL, is_finished TINYINT(1) NOT NULL, viewers_number INT DEFAULT NULL, liga VARCHAR(4) NOT NULL, UNIQUE INDEX UNIQ_7A5BC5052ABEACD6 (match_id), INDEX IDX_7A5BC5055F9117C2 (liga_group_id), INDEX IDX_7A5BC505E72BCFA4 (team1_id), INDEX IDX_7A5BC505F59E604A (team2_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `liga_match` ADD CONSTRAINT FK_7A5BC5055F9117C2 FOREIGN KEY (liga_group_id) REFERENCES liga_group (id)');
        $this->addSql('ALTER TABLE `liga_match` ADD CONSTRAINT FK_7A5BC505E72BCFA4 FOREIGN KEY (team1_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE `liga_match` ADD CONSTRAINT FK_7A5BC505F59E604A FOREIGN KEY (team2_id) REFERENCES team (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `liga_match`');
    }
}
